//FIND USERS WITH Y ON FIRST OR LAST
db.newUsers.find({ $or: [{ firstName: { $regex: 'y', $options: '$i' } }, { lastName: { $regex: 'y', $options: '$i' } }] }, { "_id": 0, "isAdmin": 1, "email": 1 })
//FIND USERS WITH WITH E ON FIRST AND AN ADMIN
db.newUsers.find({ $and: [{ firstName: { $regex: 'e', $options: '$i' } }, { isAdmin: true }] }, { "_id": 0, "email": 1, "isAdmin": 1 })
//FIND PRODUCTS WITH LETTER X ON NAME AND PRICE <= to 50000
db.products.find({ $and: [{ name: { $regex: 'x', $options: '$i' } }, { price: { $gte: 50000 } }] })
//UPDATE ALL PRODUCTS WITH PRICE LESS THAN 2000 to INACTIVE
db.products.updateMany({ price: { $lt: 2000 } }, { $set: { isActive: false } })
//DELETE ALL PRODUCTS WITH PRICE GREATER THAN 20000
db.products.deleteMany({ price: { $gt: 20000 } })